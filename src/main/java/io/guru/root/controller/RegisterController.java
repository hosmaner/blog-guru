package io.guru.root.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.guru.root.entity.User;
import io.guru.root.service.UserService;

@Controller
@RequestMapping("/register")
public class RegisterController {

	public RegisterController(){}
	@Autowired
	private UserService userService;


	@ModelAttribute("user")
	public User constructUser(){
		return new User();
	} 

	@RequestMapping
	public String showRegester() {
		return "user-register";
	}
	@RequestMapping(method=RequestMethod.POST)
	public String doRegester(@Valid @ModelAttribute("user") User user, BindingResult result) {
		if(result.hasErrors())
			return "user-register";
		userService.save(user);
		return "redirect:/register.html?success=true";
	}
	
	@RequestMapping(value="/available")
	@ResponseBody
	public String available(@RequestParam String userName){
	Boolean available=	userService.findOne(userName)==null;
		return available.toString();
		}


}
