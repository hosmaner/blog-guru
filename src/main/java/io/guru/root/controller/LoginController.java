/**
 * 
 */
package io.guru.root.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Achuth
 *
 */
@Controller
public class LoginController {

	/**
	 * 
	 */
	public LoginController() {
	}

	@RequestMapping("/login")
	public String	login(){
		return "user-login";
	}
}
