/**
 * 
 */
package io.guru.root.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.guru.root.entity.Blog;
import io.guru.root.entity.User;
import io.guru.root.service.BlogService;
import io.guru.root.service.UserService;

/**
 * @author hanuman
 *
 */
@Controller
public class UserController {

	/**
	 * 
	 */
	public UserController() {
		// TODO Auto-generated constructor stub
	}
	@Autowired
	private UserService userService;

	@Autowired
	private BlogService blogService;

	@ModelAttribute("blog")
	public Blog constructBlog(){
		return new Blog();
	} 

	/**
	 * @param userService
	 *            the userService to set
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}




	@RequestMapping("/account")
	public String account(Model model,Principal principal ){
		String name=principal.getName();
		model.addAttribute("user",userService.findOneByBlogs(name));
		return "account";

	}
	@RequestMapping(value="/account",method=RequestMethod.POST)
	public String doAddBlog(Model model,@Valid @ModelAttribute("blog")Blog blog, Principal principal ,BindingResult result){
		if(result.hasErrors())
			return account(model, principal);

		String name= principal.getName();
		blogService.save(blog, name);
		return "redirect:/account.html";
	}
	@RequestMapping(value="/blog/remove/{id}")
	public String removeBlog(@PathVariable Long id){
		Blog blog= blogService.findOne(id);
		blogService.delete(blog);
		return "redirect:/account.html";
	}


}
