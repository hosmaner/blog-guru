/**
 * 
 */
package io.guru.root.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import io.guru.root.entity.Blog;
import io.guru.root.entity.Item;
import io.guru.root.entity.User;
import io.guru.root.exception.RssException;
import io.guru.root.repository.BlogRepository;
import io.guru.root.repository.ItemRepository;
import io.guru.root.repository.UserRepository;

/**
 * @author hanuman
 *
 */
@Service
public class BlogService {
	@Autowired
	private BlogRepository blogRepository;
	@Autowired
	private UserRepository userRepositorysitory;
	@Autowired
	private RssService rssService;
	@Autowired
	private ItemRepository itemRepository;


	public void saveItems(Blog blog){
		try {
			List<Item> items=	rssService.getItems(blog.getUrl());
			for (Item item : items) {
				Item savedItem=	itemRepository.findByBlogAndLink(blog, item.getLink());
				if(savedItem==null){
					item.setBlog(blog);
					itemRepository.save(item);
				}	
			}
		} catch (RssException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//1 hr =60sec*60min*1000
	@Scheduled(fixedDelay=60*60*1000)
	public void reloadBlogs(){
		 List<Blog> blogs = blogRepository.findAll();
		for (Blog blog : blogs) {
			saveItems(blog);
		}
	}

	public void save(Blog blog ,String name){
		User user=userRepositorysitory.findByName(name);
		blog.setUser(user);
		blogRepository.save(blog);
		saveItems(blog);
	}

	/*public void delete(Long id) {
		blogRepository.delete(id);
	}
	 */
	public Blog findOne(Long id) {
		return 		blogRepository.findOne(id);

	}
	//#blog.user.name == authentication.name or hasRole('ROLE_ADMIN')
	//@Secured("hasRole('ROLE_ADMIN') or #blog.user.name == authentication.name")
	@PreAuthorize("#blog.user.name == authentication.name or hasRole('ROLE_ADMIN')")
	public void delete(@P("blog") Blog blog) {
		blogRepository.delete(blog);
	}
}
