/**
 * 
 */
package io.guru.root.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import io.guru.root.entity.Item;
import io.guru.root.repository.ItemRepository;

/**
 * @author hanuman
 *
 */
@Service
public class ItemService {

	@Autowired
	private ItemRepository itemRepository;
	/**
	 * 
	 */
	public ItemService() {
	}

	public List<Item> getItems() {
		return itemRepository.findAll(new PageRequest(0, 20,Direction.DESC,"publishedDate")).getContent();
	}

}
