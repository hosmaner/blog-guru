/**
 * 
 */
package io.guru.root.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import io.guru.root.entity.Blog;
import io.guru.root.entity.Item;
import io.guru.root.entity.Role;
import io.guru.root.entity.User;
import io.guru.root.repository.BlogRepository;
import io.guru.root.repository.ItemRepository;
import io.guru.root.repository.RoleRepository;
import io.guru.root.repository.UserRepository;



/**
 * @author hanuman
 *
 */
@Service
@Transactional
public class UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BlogRepository blogRepository;
	@Autowired
	private ItemRepository itemRepository;
	@Autowired
	private RoleRepository roleRepository;

	public List<User> findAll(){
		return userRepository.findAll();
	}

	public  User findOne(Long id) {
		return userRepository.findOne(id);
	}

	public User findOneByBlogs(Long id){
		User user= this.findOne(id);

		List<Blog> blogs=blogRepository.findByUser(user);
		List<Item> items;
		for(Blog blog: blogs){
			items=itemRepository.findByBlog(blog, new PageRequest(0, 10, Direction.ASC, "publishedDate"));
			blog.setItems(items);
		}
		user.setBlogs(blogs);
		return user;
	}
	public void save(User user) {
		user.setEnabled(true);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(user.getPassword()));

		List<Role> roles = new ArrayList<Role>();
		roles.add(roleRepository.findByName("ROLE_USER"));
		user.setRoles(roles);

		userRepository.save(user);
	}

	public User findOneByBlogs(String name) {
		User user=userRepository.findByName(name);
		return	findOneByBlogs(user.getId());
	}

	public void delete(Long id) {
userRepository.delete(id);		
	}

	public User findOne(String userName) {
		return userRepository.findByName(userName);
	}

}
