/**
 * 
 */
package io.guru.root.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.guru.root.entity.User;

/**
 * @author hanuman
 *
 */
public interface UserRepository  extends JpaRepository<User
,Long>{

	User findByName(String name);

}
