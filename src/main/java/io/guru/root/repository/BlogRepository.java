/**
 * 
 */
package io.guru.root.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import io.guru.root.entity.Blog;
import io.guru.root.entity.User;

/**
 * @author hanuman
 *
 */
public interface BlogRepository  extends JpaRepository<Blog
,Long>{
	List<Blog> findByUser(User user);

}
