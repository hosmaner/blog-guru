/**
 * 
 */
package io.guru.root.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import io.guru.root.entity.Role;

/**
 * @author hanuman
 *
 */
@Transactional
public interface RoleRepository  extends JpaRepository<Role
,Long>{


	Role findByName(String name);

}
