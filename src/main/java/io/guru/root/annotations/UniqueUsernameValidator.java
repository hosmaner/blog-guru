/**
 * 
 */
package io.guru.root.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import io.guru.root.repository.UserRepository;

/**
 * @author hanuman
 *
 */
public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

	/**
	 * 
	 */
	public UniqueUsernameValidator() {
	}
	@Autowired
	private UserRepository userRepository;

	@Override
	public void initialize(UniqueUsername constraintAnnotation) {

	}

	@Override
	public boolean isValid(String username, ConstraintValidatorContext context) {
		if(userRepository==null){return true;}// it will be only null only for the initDB service so returning tru else it wont be true
		return userRepository.findByName(username)==null;
	}

}
