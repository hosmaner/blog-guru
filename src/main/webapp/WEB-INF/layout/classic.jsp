<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tilesx"%>
<%@ include file="../layout/taglib.jsp"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:getAsString name="title" /></title>
</head>
 
<body>
	<tilesx:useAttribute name="current" />


	<div class="container" href="myPage">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href='<spring:url value="/"/>'><span
						class="glyphicon glyphicon-fire"></span> Java Monk </a>
				</div>
				<ul class="nav navbar-nav">
					<li class="${current == 'home' ? 'active' : '' }"><a
						href="<spring:url value="/"/>"><span
							class="glyphicon glyphicon-home"></span> Home</a></li>
					<security:authorize access="hasRole('ROLE_ADMIN')">

						<li class="${current == 'users' ? 'active' : '' }"><a
							href='<spring:url value="/users.html"/>'><span
								class="glyphicon glyphicon-user"></span> Users</a></li>
					</security:authorize>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<security:authorize access="isAuthenticated()">

						<li class="${current == 'account' ? 'active' : '' }"><a
							href='<spring:url value="/account.html"/>'><span
								class="glyphicon glyphicon-user"></span> My Account</a></li>

						<li><a href='<spring:url value="/logout"/>'><span
								class="	glyphicon glyphicon-log-out"> </span> LogOut</a></li>
					</security:authorize>
					<security:authorize access="! isAuthenticated()">
						<li class="${current == 'register' ? 'active' : '' }"><a
							href='<spring:url value="/register.html"/>'><span
								class="glyphicon glyphicon-user"></span> Register</a></li>
						<li class="${current == 'login' ? 'active' : '' }"><a
							href='<spring:url value="/login.html"/>'><span
								class="glyphicon glyphicon-log-in"></span> Login</a></li>
					</security:authorize>
				</ul>
			</div>
		</nav>
	</div>
	<tiles:insertAttribute name="body" />
	<br>
	<br>


	<div class="text-center">
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>