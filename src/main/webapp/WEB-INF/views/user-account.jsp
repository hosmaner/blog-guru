<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../layout/taglib.jsp"%>



<div class="component">


	<%-- <h4 class="text-center">user name :${user.name}</h4> --%>

	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal">
		<span class="glyphicon glyphicon-plus"></span> new blog
	</button>

	<ul class="nav nav-tabs" id="blog_tabs">
		<c:forEach items="${user.blogs}" var="blog">
			<li><a data-toggle="tab" href="#blog_${blog.id}">${blog.name}</a></li>
		</c:forEach>
	</ul>


	<div class="tab-content">
		<c:forEach items="${user.blogs}" var="blog">

			<div class="tab-pane" id="blog_${blog.id}">
				<a href='<spring:url value="/blog/remove/${blog.id}.html"/>'
					class="btn btn-danger triggerRemove">remove ${blog.name} blog</a>
				<h2>${blog.name}</h2>
				<table>
					<tr>
						<th>blog id</th>
						<td>: &nbsp;</td>
						<td>${blog.id }</td>
					</tr>
					<tr>
						<th>blog name</th>
						<td>: &nbsp;</td>
						<td>${blog.name }</td>
					</tr>
					<tr>
						<th>blog url</th>
						<td>: &nbsp;</td>
						<td>${blog.url }</td>
					</tr>
				</table>


				<table
					class="table table-hover table-striped table-bordered table-hover">
					<tr>
						<th>id</th>

						<th>title</th>

						<th>description</th>

						<th>publishedDate</th>

						<th>link</th>
					</tr>
					<c:forEach items="${blog.items}" var="item">
						<tr>
							<td>${item.id }</td>

							<td>${item.title }</td>

							<td>${item.description }</td>

							<td>${item.publishedDate }</td>

							<td>${item.link }</td>
						</tr>

					</c:forEach>
				</table>


			</div>
		</c:forEach>

	</div>






	<form:form commandName="blog" cssClass="form-horizontal blogForm">
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">New Blog</h4>
					</div>
					<div class="modal-body">


						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">Blog
								Name</label>
							<div class="col-sm-10">
								<form:input cssClass="form-control" id="inputEmail3"
									placeholder="name" path="name" />
								<form:errors path="name" />
							</div>
						</div>

						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">Blog URL</label>
							<div class="col-sm-10">
								<form:input cssClass="form-control" id="inputEmail3"
									placeholder="url" path="url" />
								<form:errors path="url" />
							</div>
						</div>


					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" value="save">

					</div>
				</div>
			</div>
		</div>
	</form:form>


</div>


<!-- Modal -->
<div class="modal fade" id="modalRemove" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Remove Blog</h4>
			</div>
			<div class="modal-body">really remove?</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">cancle</button>
				<a href="" class="btn btn-danger removeBtn">Remove</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(
			function() {
				$('#blog_tabs a:first').tab('show');//select the first tab source :: bootstrap javascript tabs 

				$(".triggerRemove").click(
						function(e) {
							alert("56544545454545454");
							e.preventDefault();
							$("#modalRemove .removeBtn").attr("href",
									$(this).attr("href"));
							$("#modalRemove").modal();
						});

				$(".blogForm").validate(
						{
							rules : {
								name : {
									required : true,
									minlength : 3
								},
								url : {
									required : true,
									url : true
								}
							},
							highlight : function(element) {
								$(element).closest(".form-group").removeClass(
										"has-success").addClass("has-error");
							},
							unhighlight : function(element) {
								$(element).closest(".form-group").removeClass(
										"has-error").addClass("has-success");
							}
						});

			});
</script>
